﻿# Bookmarks for Pali Chanting Book

There is a nice [Pali Chanting
compilation](https://www.basicbuddhism.org/resources/PaliChantingBook.pdf) with
therefore broken PDF bookmarks.

Repository contains actual bookmark metadata in [pdftk
format](https://www.pdflabs.com/blog/export-and-import-pdf-bookmarks/). PDF
file may be updated with command:

```bash
pdftk PaliChantingBook.pdf update_info_utf8 pdftk_bookmarks_utf8.txt output PaliChantingBook_updated.pdf compress
```
